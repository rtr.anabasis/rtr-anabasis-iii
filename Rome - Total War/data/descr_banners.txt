
banner		roman
model		data/models/standard_general_julii.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas
model		data/models/standard_julii.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_units.cas
model		data/models/standard_general_julii.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_general.cas

banner		barbarian
model		data/models/standard_general_gaul.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_barb_general.cas
model		data/models/standard_gaul.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_barb_units.cas
model		data/models/standard_general_gaul.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_barb_general.cas

banner		carthaginian
model		data/models/standard_general_carthage.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas
model		data/models/standard_carthage.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_units.cas
model		data/models/standard_general_carthage.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas

banner		greek
model		data/models/standard_general_macedonia.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas
model		data/models/standard_macedonia.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_units.cas
model		data/models/standard_general_macedonia.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas

banner		egyptian
model		data/models/standard_general_egypt.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas
model		data/models/standard_julii.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_units.cas
model		data/models/standard_general_egypt.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_general.cas

banner		eastern
model		data/models/standard_general_julii.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_eastern.cas
model		data/models/standard_julii.cas
skeleton	standard_legion_pole
outline		data/models/standard_battle_glow_eastern_units.cas
model		data/models/standard_general_julii.cas
skeleton	general_legion_pole
outline		data/models/standard_battle_glow_eastern.cas


faction				rome
standard_texture	models/textures/standard_rome.tga
rebels_texture		models/textures/standard_slave_romans.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_rome_ally.tga

faction				pergamon
standard_texture	models/textures/standard_pergamon.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_pergamon_ally.tga

faction				baktria
standard_texture	models/textures/standard_baktria.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_baktria_ally.tga

faction				maurya
standard_texture	models/textures/standard_maurya.tga
rebels_texture		models/textures/standard_slave_eastern.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_maurya_ally.tga

faction				macedon
standard_texture	models/textures/standard_macedon.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_macedon_ally.tga

faction				egypt
standard_texture	models/textures/standard_egypt.tga
rebels_texture		models/textures/standard_slave_egypt.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_egypt_ally.tga

faction				seleucid
standard_texture	models/textures/standard_seleucid.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_seleucid_ally.tga

faction				carthage
standard_texture	models/textures/standard_carthage.tga
rebels_texture		models/textures/standard_slave_carthaginian.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_carthage_ally.tga

faction				parthia
standard_texture	models/textures/standard_parthia.tga
rebels_texture		models/textures/standard_slave_eastern.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_parthia_ally.tga

faction				pontus
standard_texture	models/textures/standard_pontus.tga
rebels_texture		models/textures/standard_slave_eastern.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_pontus_ally.tga

faction				gauls
standard_texture	models/textures/standard_gauls.tga
rebels_texture		models/textures/standard_slave_barbarian.tga
routing_texture		models/textures/standard_routing_barb.tga
ally_texture		models/textures/standard_gauls_ally.tga

faction				germans
standard_texture	models/textures/standard_germans.tga
rebels_texture		models/textures/standard_slave_barbarian.tga
routing_texture		models/textures/standard_routing_barb.tga
ally_texture		models/textures/standard_germans_ally.tga

faction				britons
standard_texture	models/textures/standard_britons.tga
rebels_texture		models/textures/standard_slave_barbarian.tga
routing_texture		models/textures/standard_routing_barb.tga
ally_texture		models/textures/standard_britons_ally.tga

faction				armenia
standard_texture	models/textures/standard_armenia.tga
rebels_texture		models/textures/standard_slave_eastern.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_armenia_ally.tga

faction				nabatea
standard_texture	models/textures/standard_nabatea.tga
rebels_texture		models/textures/standard_slave_carthaginian.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_nabatea_ally.tga

faction				sparta
standard_texture	models/textures/standard_sparta.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_sparta_ally.tga

faction				saba
standard_texture	models/textures/standard_saba.tga
rebels_texture		models/textures/standard_slave_carthaginian.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_saba_ally.tga

faction				bosporan
standard_texture	models/textures/standard_scythia.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_scythia_ally.tga

faction				spain
standard_texture	models/textures/standard_spain.tga
rebels_texture		models/textures/standard_slave_barbarian.tga
routing_texture		models/textures/standard_routing_barb.tga
ally_texture		models/textures/standard_spain_ally.tga

faction				emporium
standard_texture	models/textures/standard_illyria.tga
rebels_texture		models/textures/standard_slave_greeks.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_illyria_ally.tga

faction				slave
standard_texture	models/textures/standard_slave.tga
rebels_texture		models/textures/standard_slave.tga
routing_texture		models/textures/standard_routing_all.tga
ally_texture		models/textures/standard_slave_ally.tga
