; Rome Total Realism Anabasis III

Gades_Poeni
	Gades
	carthage
	Poeni
	111 187 219
	dogs, pottery, textiles, slaves
	5
	3

Carteia_Poeni
	Carteia
	spain
	Poeni
	227 86 199
	purple_dye, wine, copper, timber, slaves
	5
	3

Malaca_Poeni
	Malaca
	spain
	Poeni
	227 219 86
	dogs, slaves
	5
	3

Sexi_Poeni
	Sexi
	spain
	Poeni
	139 227 86
	purple_dye, slaves
	5
	3

Abdera_Poeni
	Abdera
	spain
	Poeni
	227 86 86
	dogs, hides, wine, slaves
	5
	3

Barea_Poeni
	Barea
	spain
	Poeni
	106 86 227
	pottery, lead, marble, slaves
	5
	3

Bastuli_I
	Hasta
	spain
	Turdetani
	159 46 46
	olive_oil, slaves
	5
	3

Bastuli_II
	Astapa
	spain
	Turdetani
	122 223 126
	hides, olive_oil, slaves
	5
	3

Bastuli_III
	Iliberis
	spain
	Turdetani
	165 78 76
	pigs, pottery, olive_oil, wine, slaves
	5
	3

Bastetani_I
	Basti
	spain
	Bastetani
	227 216 86
	pottery, hides, wild_animals, wine, marble, slaves
	5
	3

Bastetani_II
	Ilorcis
	spain
	Bastetani
	136 132 71
	pigs, textiles, silver, slaves
	5
	3

Bastetani_III
	Mastia
	spain
	Bastetani
	227 186 86
	dogs, textiles, silver, slaves
	5
	3

Turdetani_I
	Hispalis
	spain
	Turdetani
	220 221 80
	pigs, wine, furs, copper, timber, slaves
	5
	3

Turdetani_II
	Corduba
	spain
	Turdetani
	222 162 91
	pottery, olive_oil, furs, slaves
	5
	3

Oretani_I
	Castulo
	spain
	Oretani
	80 165 76
	pottery, hides, furs, silver, slaves
	5
	3

Oretani_II
	Salaria
	spain
	Oretani
	53 66 100
	wild_animals, silver, slaves
	5
	3

Contestani_I
	Sigisa
	spain
	Edetani
	217 139 181
	hides, slaves
	5
	3

Contestani_II
	Lucentum
	spain
	Edetani
	86 227 226
	textiles, iron, slaves
	5
	3

Turduli_I
	Ugultunia
	spain
	Lusitani
	178 137 154
	hides, timber, slaves
	5
	3

Turduli_II
	Mellaria
	spain
	Lusitani
	19 144 159
	pigs, olive_oil, silver, slaves
	5
	3

Carpetani_I
	Oretum
	spain
	Oretani
	127 137 17
	hides, timber, slaves
	5
	3

Carpetani_II
	Mentesa
	spain
	Oretani
	159 19 72
	wild_animals, silver, slaves
	5
	3

Edetani_I
	Libisosa
	spain
	Edetani
	116 227 86
	pigs, furs, slaves
	5
	3

Edetani_II
	Saguntum
	spain
	Edetani
	172 86 227
	pigs, pottery, hides, furs, iron, slaves
	5
	3
