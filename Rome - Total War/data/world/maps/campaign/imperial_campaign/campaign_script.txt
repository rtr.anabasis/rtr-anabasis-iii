;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; RTRA III Campaign Script ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

script
suspend_during_battle on

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Campaign Perfect Spy ;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

console_command toggle_perfect_spy

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; Campaign Name Characters ;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; DEAD CHARACTERS
; --------------------------------------

console_command kill_character "M00_Attalos S02_Pergamenos"
console_command kill_character "M00_Philetairos S00_Pergamenos"
console_command kill_character "M00_Mahinda S00_Indikos"
console_command kill_character "M01_Demetrios S01_Makedonios"
console_command kill_character "M00_Eudamidas S01_Spartiates"
console_command kill_character "M00_Leonidas S00_Spartiates"
console_command kill_character "M01_Pairysades S00_Kimmerios"
console_command kill_character "M00_Khremonides S00_Athenaios"
console_command kill_character "M00_Magas S00_Kyrenaios"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Campaign AI Buildings ;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; AI BUILDINGS SELEUCID - 250 BC
; --------------------------------------

if not I_LocalFaction seleucid

console_command create_building Ephesos shrine_of_hunting_temple
console_command create_building Ephesos shrine_of_healing_shrine

console_command create_building Apameia shrine_of_trade_temple
console_command create_building Apameia shrine_of_hunting_shrine

console_command create_building Tarsos shrine_of_fertility_temple
console_command create_building Tarsos shrine_of_hunting_shrine

console_command create_building Ariaratheia shrine_of_trade_temple
console_command create_building Ariaratheia shrine_of_hunting_shrine

console_command create_building Gazaka shrine_of_fertility_temple
console_command create_building Gazaka shrine_of_violence_shrine

console_command create_building Ekbatana shrine_of_fertility_temple
console_command create_building Ekbatana shrine_of_healing_shrine

console_command create_building Rhagai shrine_of_fertility_temple
console_command create_building Rhagai shrine_of_hunting_shrine

console_command create_building Hekatompylos shrine_of_trade_temple
console_command create_building Hekatompylos shrine_of_hunting_shrine

console_command create_building Harmozeia shrine_of_fertility_temple
console_command create_building Harmozeia shrine_of_violence_shrine

console_command create_building Issatis shrine_of_fun_temple
console_command create_building Issatis shrine_of_hunting_shrine

console_command create_building Alexandreia_III shrine_of_trade_temple
console_command create_building Alexandreia_III shrine_of_healing_shrine

console_command create_building Antiokheia_I shrine_of_trade_shrine
console_command create_building Antiokheia_I shrine_of_sailing_shrine

console_command create_building Nisibis shrine_of_trade_shrine
console_command create_building Nisibis shrine_of_violence_shrine

console_command create_building Sousa shrine_of_fun_shrine
console_command create_building Sousa shrine_of_sailing_shrine

console_command create_building Persepolis shrine_of_trade_shrine
console_command create_building Persepolis shrine_of_healing_shrine

end_if

; AI BUILDINGS CARTHAGE - 250 BC
; --------------------------------------

if not I_LocalFaction carthage

console_command create_building Gades shrine_of_fertility_temple
console_command create_building Gades shrine_of_sailing_shrine

end_if

; AI BUILDINGS EMPORIUM - 250 BC
; --------------------------------------

if not I_LocalFaction emporium

console_command create_building Khersonessos shrine_of_fun_temple
console_command create_building Khersonessos shrine_of_sailing_shrine

end_if

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; Campaign AI Armies ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; AI ARMIES ROME - 250 BC
; --------------------------------------

if not I_LocalFaction rome

spawn_army
faction rome

; Region: SICILIA I
; --------------------------------------
character	M00_Gaius S00_Atilius, named character, command 0, influence 0, management 0, subterfuge 0, age 42, , x 158, y 81
unit		RTRA rome cavalry general early			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry triarii			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry triarii			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome infantry triarii			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome infantry principes			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry principes			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry principes			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome infantry principes			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome infantry hastati			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry hastati			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry hastati			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome infantry hastati			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome infantry hastati			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome auxiliary velites			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome auxiliary velites			exp 1 armour 0 weapon_lvl 0
unit		RTRA rome auxiliary velites			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome auxiliary velites			exp 0 armour 0 weapon_lvl 0
unit		RTRA rome cavalry alae				exp 1 armour 0 weapon_lvl 0
unit		RTRA rome cavalry equites			exp 0 armour 0 weapon_lvl 0
unit		RTRA generic engine ballista light		exp 0 armour 0 weapon_lvl 0
end

;console_command give_trait "M00_Gaius S00_Atilius" XXXXX
;console_command give_trait "M00_Gaius S00_Atilius" XXXXX
;console_command give_trait "M00_Gaius S00_Atilius" XXXXX
;console_command give_trait "M00_Gaius S00_Atilius" XXXXX
;console_command give_trait "M00_Gaius S00_Atilius" XXXXX

;console_command give_ancillary "M00_Gaius S00_Atilius" XXXXX
;console_command give_ancillary "M00_Gaius S00_Atilius" XXXXX
;console_command give_ancillary "M00_Gaius S00_Atilius" XXXXX
;console_command give_ancillary "M00_Gaius S00_Atilius" XXXXX
;console_command give_ancillary "M00_Gaius S00_Atilius" XXXXX

end_if

; AI ARMIES CARTHAGE - 250 BC
; --------------------------------------

if not I_LocalFaction carthage

spawn_army
faction carthage

; Region: SICILIA III
; --------------------------------------
character	M00_Bodeshmun S00_Phoenicus, named character, command 0, influence 0, management 0, subterfuge 0, age 38, , x 157, y 81
unit		RTRA carthage cavalry general			exp 1 armour 0 weapon_lvl 0
unit		RTRA merc infantry devoti			exp 0 armour 0 weapon_lvl 0
unit		RTRA carthage infantry hoplitai			exp 1 armour 0 weapon_lvl 0
unit		RTRA carthage infantry hoplitai			exp 0 armour 0 weapon_lvl 0
unit		RTRA merc infantry celtiberi			exp 1 armour 0 weapon_lvl 0
unit		RTRA merc infantry celtiberi			exp 0 armour 0 weapon_lvl 0
unit		RTRA merc infantry scutarii			exp 1 armour 0 weapon_lvl 0
unit		RTRA merc infantry scutarii			exp 0 armour 0 weapon_lvl 0
unit		RTRA carthage infantry ekdromoi			exp 1 armour 0 weapon_lvl 0
unit		RTRA carthage infantry ekdromoi			exp 1 armour 0 weapon_lvl 0
unit		RTRA carthage infantry ekdromoi			exp 0 armour 0 weapon_lvl 0
unit		RTRA aor africa infantry ekdromoi		exp 1 armour 0 weapon_lvl 0
unit		RTRA aor africa infantry ekdromoi		exp 0 armour 0 weapon_lvl 0
unit		RTRA aor africa auxiliary akontistai		exp 1 armour 0 weapon_lvl 0
unit		RTRA aor africa auxiliary akontistai		exp 0 armour 0 weapon_lvl 0
unit		RTRA unique auxiliary gimnitai			exp 1 armour 0 weapon_lvl 0
unit		RTRA unique auxiliary gimnitai			exp 0 armour 0 weapon_lvl 0
unit		RTRA aor africa cavalry hippakontistai		exp 1 armour 0 weapon_lvl 0
unit		RTRA aor africa cavalry hippakontistai		exp 0 armour 0 weapon_lvl 0
unit		RTRA merc cavalry devoti			exp 0 armour 0 weapon_lvl 0
end

;console_command give_trait "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_trait "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_trait "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_trait "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_trait "M00_Bodeshmun S00_Phoenicus" XXXXX

;console_command give_ancillary "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_ancillary "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_ancillary "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_ancillary "M00_Bodeshmun S00_Phoenicus" XXXXX
;console_command give_ancillary "M00_Bodeshmun S00_Phoenicus" XXXXX

end_if

; AI ARMIES SPARTA - 250 BC
; --------------------------------------

if not I_LocalFaction sparta

spawn_army
faction sparta

; Region: LAKONIKE
; --------------------------------------
character	M00_Pantheos S00_Spartiates, named character, command 0, influence 0, management 0, subterfuge 0, age 18, , x 200, y 93
unit		RTRA sparta infantry general			exp 1 armour 0 weapon_lvl 0
unit		RTRA sparta infantry homoioi			exp 0 armour 0 weapon_lvl 0
unit		RTRA sparta infantry homoioi			exp 0 armour 0 weapon_lvl 0
unit		RTRA sparta infantry perioikoi			exp 1 armour 0 weapon_lvl 0
unit		RTRA sparta infantry perioikoi			exp 0 armour 0 weapon_lvl 0
unit		RTRA sparta auxiliary akontistai		exp 1 armour 0 weapon_lvl 0
unit		RTRA sparta auxiliary akontistai		exp 0 armour 0 weapon_lvl 0
end

;console_command give_trait "M00_Pantites S00_Spartiates" XXXXX
;console_command give_trait "M00_Pantites S00_Spartiates" XXXXX
;console_command give_trait "M00_Pantites S00_Spartiates" XXXXX
;console_command give_trait "M00_Pantites S00_Spartiates" XXXXX
;console_command give_trait "M00_Pantites S00_Spartiates" XXXXX

;console_command give_ancillary "M00_Pantites S00_Spartiates" XXXXX
;console_command give_ancillary "M00_Pantites S00_Spartiates" XXXXX
;console_command give_ancillary "M00_Pantites S00_Spartiates" XXXXX
;console_command give_ancillary "M00_Pantites S00_Spartiates" XXXXX
;console_command give_ancillary "M00_Pantites S00_Spartiates" XXXXX

end_if

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; SCRIPT END ;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

campaign_wait 1
console_command clear_messages

advance_advice_thread Background_Script_Thread
campaign_wait 1

select_ui_element advisor_portrait_button
simulate_mouse_click lclick_up

end_script
