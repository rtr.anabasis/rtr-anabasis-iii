;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

climate			default

stone_road		0.75
mud_road		0.70
grass_short		0.70
grass_long		0.65
forest_dense		0.65
scrub_dense		0.60
rock			0.60
mud			0.55
sand			0.55
snow			0.50
swamp			0.50
water			0.45
ice			0.45

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;