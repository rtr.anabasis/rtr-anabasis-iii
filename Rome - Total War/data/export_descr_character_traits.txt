;ROME TOTAL REALISM ANABASIS III

;===============================================================
;== TRAIT DATA STARTS HERE ==
;===============================================================

;------------------------------------------
Trait Alexander
    Characters family
    Hidden

    Level Alexander
        Description Alexander_desc
        EffectsDescription Alexander_effects_desc
        Threshold  1 

;------------------------------------------
Trait Immortal
    Characters family
    Hidden

    Level Immortal
        Description Immortal_desc
        EffectsDescription Immortal_effects_desc
        Threshold  1 

;------------------------------------------
Trait Immovable
    Characters family
    Hidden

    Level Immovable
        Description Immovable_desc
        EffectsDescription Immovable_effects_desc
        Threshold  1 

        Effect MovementPoints  -500

;-----------------------------------------
