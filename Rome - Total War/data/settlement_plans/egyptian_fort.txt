plan

{

target rectangle 	x 0 z 0 rot 0 width 32 height 32
floor			egyptian_camp_underlay.CAS roman_camp_overlay.CAS

ground_types		roman_camp_ground.tga -160 -160 160 160
street_plan		roman_camp_pathfinding.CAS
borders			

{


;WALLS

gateway,			-0.00,	96.00,	-180,	0.00
gateway,			-96.00,	0.00,	-90.00,	0.00
gateway,			96.00,	0.00,	90.00,	0.00
gateway,			0.00,	-96.00,	0.00,	0.00
straight,			-96.00,	64.00,	-90.00,	0.00
straight,			-64.00,	96.00,	180.00,	0.00
straight,			64.00,	96.00,	180.00,	0.00
straight,			64.00,	-96.00,	0.00,	0.00
straight,			-64.00,	-96.00,	0.00,	0.00
straight,			-96.00,	-64.00,	-90.00,	0.00
straight,			96.00,	-64.00,	90.00,	0.00
straight,			96.00,	64.00,	90.00,	0.00
tower_external_corner_link,	96.00,	-96.00,	90.00,	0.00
tower_external_corner_link,	-96.00,	-96.00,	0.00,	0.00
tower_external_corner_link,	96.00,	96.00,	-180,	0.00
tower_external_corner_link,	-96.00,	96.00,	-90.00,	0.00

}


;BUILDINGS	

stove,			0.00,	40.00,	0.00,	0.00
generals_stake_fence,	0.00,	40.00,	0.00,	-1.00

pile_of_logs,		10.00,	30.00,	-55.00,	0.00
pile_of_logs,		12.00,	32.00,	0.00,	0.00

centurions_tent,	-44.00,	-52.00,	90.00,	0.00
centurions_tent,	44.00,	-52.00,	-90.00,	0.00
centurions_tent,	20.00,	-52.00,	90.00,	0.00
centurions_tent,	12.00,	-52.00,	-90.00,	0.00
centurions_tent,	44.00,	52.00,	-90.00,	0.00
centurions_tent,	-12.00,	-52.00,	90.00,	0.00
centurions_tent,	-20.00,	-52.00,	-90.00,	0.00
centurions_tent,	-44.00,	52.00,	90.00,	0.00
centurions_tent,	20.00,	52.00,	90.00,	0.00
centurions_tent,	-20.00,	52.00,	-90.00,	0.00

generals_side_tent,	-11.00,	41.00,	90.00,	0.00
generals_side_tent,	11.00,	41.00,	-90.00,	0.00

generals_tent,		0.00,	51.00,	0.00,	0.00

legionary_tent_both_flaps_closed,	-44.00,	-44.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	20.00,	-44.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	12.00,	-12.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	12.00,	-44.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	44.00,	-12.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	44.00,	-44.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	44.00,	44.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	-12.00,	-44.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	-20.00,	-12.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	-20.00,	-44.00,	-90.00,	0.00
legionary_tent_both_flaps_closed,	-44.00,	12.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	-44.00,	44.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	20.00,	44.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	20.00,	12.00,	90.00,	0.00
legionary_tent_both_flaps_closed,	-20.00,	44.00,	-90.00,	0.00

legionary_tent_both_flaps_open,		-20.00,	-28.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		-44.00,	-36.00,	90.00,	0.00
legionary_tent_both_flaps_open,		-44.00,	-12.00,	90.00,	0.00
legionary_tent_both_flaps_open,		12.00,	-28.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		20.00,	-12.00,	90.00,	0.00
legionary_tent_both_flaps_open,		20.00,	-36.00,	90.00,	0.00
legionary_tent_both_flaps_open,		44.00,	-28.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		44.00,	12.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		44.00,	36.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		-12.00,	-36.00,	90.00,	0.00
legionary_tent_both_flaps_open,		-12.00,	-12.00,	90.00,	0.00
legionary_tent_both_flaps_open,		20.00,	28.00,	90.00,	0.00
legionary_tent_both_flaps_open,		-20.00,	12.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		-20.00,	36.00,	-90.00,	0.00
legionary_tent_both_flaps_open,		-44.00,	28.00,	90.00,	0.00

legionary_tent_left_flap_open,		-12.00,	-28.00,	90.00,	0.00
legionary_tent_left_flap_open,		-20.00,	-36.00,	-90.00,	0.00
legionary_tent_left_flap_open,		-44.00,	36.00,	90.00,	0.00
legionary_tent_left_flap_open,		20.00,	36.00,	90.00,	0.00
legionary_tent_left_flap_open,		-20.00,	28.00,	-90.00,	0.00
legionary_tent_left_flap_open,		-44.00,	-28.00,	90.00,	0.00
legionary_tent_left_flap_open,		20.00,	-28.00,	90.00,	0.00
legionary_tent_left_flap_open,		12.00,	-36.00,	-90.00,	0.00
legionary_tent_left_flap_open,		44.00,	-36.00,	-90.00,	0.00
legionary_tent_left_flap_open,		44.00,	28.00,	-90.00,	0.00

legionary_tent_right_flap_open,		-44.00,	-20.00,	90.00,	0.00
legionary_tent_right_flap_open,		20.00,	-20.00,	90.00,	0.00
legionary_tent_right_flap_open,		44.00,	-20.00,	-90.00,	0.00
legionary_tent_right_flap_open,		12.00,	-20.00,	-90.00,	0.00
legionary_tent_right_flap_open,		44.00,	20.00,	-90.00,	0.00
legionary_tent_right_flap_open,		-12.00,	-20.00,	90.00,	0.00
legionary_tent_right_flap_open,		-20.00,	-20.00,	-90.00,	0.00
legionary_tent_right_flap_open,		-44.00,	20.00,	90.00,	0.00
legionary_tent_right_flap_open,		20.00,	20.00,	90.00,	0.00
legionary_tent_right_flap_open,		-20.00,	20.00,	-90.00,	0.00

stake_palisade_straight,		-0.00,	75.00,	-180,	-4.00
stake_palisade_straight,		-75.00,	0.00,	-90.00,	-4.00
stake_palisade_straight,		75.00,	0.00,	90.00,	-4.00
stake_palisade_straight,		0.00,	-75.00,	0.00,	-4.00

cart_B,		-50.00,	-45.00,	75.00,	0.00
cart_B,		50.00,	40.00,	30.00,	0.00

;legion_flag,				4.00,	46.00,	0.00,	0.00
;legion_standard,			-4.00,	46.00,	0.00,	0.00


;COLLISION OBJECTS

roman_camp_collision,	0,	0,	0,	0,

}
