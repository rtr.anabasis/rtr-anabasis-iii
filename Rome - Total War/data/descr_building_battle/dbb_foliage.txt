	tree_abies_alba1_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_abies_alba1_summer
			physical_info	RTRA_abies_alba1_summer.cas
		}
	}

	tree_abies_alba1_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_abies_alba1_winter
			physical_info	RTRA_abies_alba1_winter.cas
		}
	}

	tree_abies_alba2_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_abies_alba2_summer
			physical_info	RTRA_abies_alba2_summer.cas
		}
	}

	tree_abies_alba2_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_abies_alba2_winter
			physical_info	RTRA_abies_alba2_winter.cas
		}
	}

	tree_arid_bush
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_arid_bush
			physical_info	RTRA_arid_bush.cas
		}
	}

	tree_betula_alba_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_betula_alba_summer
			physical_info	RTRA_betula_alba_summer.cas
		}
	}

	tree_betula_alba_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_betula_alba_winter
			physical_info	RTRA_betula_alba_winter.cas
		}
	}

	tree_betula_pendula3_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_betula_pendula3_summer
			physical_info	RTRA_betula_pendula3_summer.cas
		}
	}

	tree_betula_pendula3_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_betula_pendula3_winter
			physical_info	RTRA_betula_pendula3_winter.cas
		}
	}

	tree_betula_pendula4_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_betula_pendula4_summer
			physical_info	RTRA_betula_pendula4_summer.cas
		}
	}

	tree_betula_pendula4_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_betula_pendula4_winter
			physical_info	RTRA_betula_pendula4_winter.cas
		}
	}

	tree_cedrus1
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_cedrus1
			physical_info	RTRA_cedrus1.cas
		}
	}

	tree_cedrus2
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_cedrus2
			physical_info	RTRA_cedrus2.cas
		}
	}

	tree_cupressus1
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_cupressus1
			physical_info	RTRA_cupressus1.cas
		}
	}

	tree_cupressus2
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_cupressus2
			physical_info	RTRA_cupressus2.cas
		}
	}

	tree_fagus_sylvatica1_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_fagus_sylvatica1_summer
			physical_info	RTRA_fagus_sylvatica1_summer.cas
		}
	}

	tree_fagus_sylvatica1_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_fagus_sylvatica1_winter
			physical_info	RTRA_fagus_sylvatica1_winter.cas
		}
	}

	tree_fagus_sylvatica2_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_fagus_sylvatica2_summer
			physical_info	RTRA_fagus_sylvatica2_summer.cas
		}
	}

	tree_fagus_sylvatica2_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_fagus_sylvatica2_winter
			physical_info	RTRA_fagus_sylvatica2_winter.cas
		}
	}

	tree_forest_bush1_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_forest_bush1_summer
			physical_info	RTRA_forest_bush1_summer.cas
		}
	}

	tree_forest_bush1_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_forest_bush1_winter
			physical_info	RTRA_forest_bush1_winter.cas
		}
	}

	tree_forest_bush2_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_forest_bush2_summer
			physical_info	RTRA_forest_bush2_summer.cas
		}
	}

	tree_forest_bush2_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_forest_bush2_winter
			physical_info	RTRA_forest_bush2_winter.cas
		}
	}

	tree_juglans_regia2_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_juglans_regia2_summer
			physical_info	RTRA_juglans_regia2_summer.cas
		}
	}

	tree_juglans_regia2_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_juglans_regia2_winter
			physical_info	RTRA_juglans_regia2_winter.cas
		}
	}

	tree_medi_bush1
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_medi_bush1
			physical_info	RTRA_medi_bush1.cas
		}
	}

	tree_medi_bush2
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_medi_bush2
			physical_info	RTRA_medi_bush2.cas
		}
	}

	tree_olea_europaea
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_olea_europaea
			physical_info	RTRA_olea_europaea.cas
		}
	}

	tree_palm_bush
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_palm_bush
			physical_info	RTRA_palm_bush.cas
		}
	}

	tree_palm_dead
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_palm_dead
			physical_info	RTRA_palm_dead.cas
		}
	}

	tree_phoenix_dactylifera1
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_phoenix_dactylifera1
			physical_info	RTRA_phoenix_dactylifera1.cas
		}
	}

	tree_phoenix_dactylifera2
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_phoenix_dactylifera2
			physical_info	RTRA_phoenix_dactylifera2.cas
		}
	}

	tree_phoenix_dactylifera3
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_phoenix_dactylifera3
			physical_info	RTRA_phoenix_dactylifera3.cas
		}
	}

	tree_phoenix_dactylifera4
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_phoenix_dactylifera4
			physical_info	RTRA_phoenix_dactylifera4.cas
		}
	}

	tree_pinus_pinea2
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_pinus_pinea2
			physical_info	RTRA_pinus_pinea2.cas
		}
	}

	tree_pinus_pinea3
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_pinus_pinea3
			physical_info	RTRA_pinus_pinea3.cas
		}
	}

	tree_prunus_armeniaca_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_prunus_armeniaca_summer
			physical_info	RTRA_prunus_armeniaca_summer.cas
		}
	}

	tree_prunus_armeniaca_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_prunus_armeniaca_winter
			physical_info	RTRA_prunus_armeniaca_winter.cas
		}
	}

	tree_prunus_dulcis_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_prunus_dulcis_summer
			physical_info	RTRA_prunus_dulcis_summer.cas
		}
	}

	tree_prunus_dulcis_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_prunus_dulcis_winter
			physical_info	RTRA_prunus_dulcis_winter.cas
		}
	}

	tree_quercus_suber2
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_quercus_suber2
			physical_info	RTRA_quercus_suber2.cas
		}
	}

	tree_ulmus_minor_summer
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_ulmus_minor_summer
			physical_info	RTRA_ulmus_minor_summer.cas
		}
	}

	tree_ulmus_minor_winter
	{
		stat_cat small_wooden
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		tree_ulmus_minor_winter
			physical_info	RTRA_ulmus_minor_winter.cas
		}
	}

	grass_A
	{
		stat_cat indestructable
		localised_name none
		level
		{
			min_health 0
			battle_stats
			item		grass_A
			physical_info	none
		}
	}
