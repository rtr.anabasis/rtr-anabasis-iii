faction						rome
culture						roman
symbol						models_strat/symbol_julii.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 102, green 31, blue 61
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_rome.tga
standard_index					0
logo_index					23
small_logo_index				248
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						pergamon
culture						greek
symbol						models_strat/symbol_brutii.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 76, green 153, blue 46
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_pergamon.tga
standard_index					1
logo_index					24
small_logo_index				249
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						baktria
culture						greek
symbol						models_strat/symbol_scipii.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 102, green 31, blue 31
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_baktria.tga
standard_index					2
logo_index					22
small_logo_index				246
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						maurya
culture						eastern
symbol						models_strat/symbol_senate.CAS
rebel_symbol					models_strat/symbol_eastern_rebel.CAS
primary_colour					red 153, green 76, blue 46
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_maurya.tga
standard_index					3
logo_index					232
small_logo_index				244
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						macedon
culture						greek
symbol						models_strat/symbol_macedon.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 31, green 102, blue 101
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_macedon.tga
standard_index					4
logo_index					239
small_logo_index				253
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						egypt
culture						egyptian
symbol						models_strat/symbol_egypt.CAS
rebel_symbol					models_strat/symbol_egypt_rebel.CAS
primary_colour					red 204, green 183, blue 61
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_egypt.tga
standard_index					5
logo_index					245
small_logo_index				257
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						seleucid
culture						greek
symbol						models_strat/symbol_seleucid.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 31, green 41, blue 102
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_seleucid.tga
standard_index					6
logo_index					242
small_logo_index				259
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						carthage
culture						carthaginian
symbol						models_strat/symbol_carthage.CAS
rebel_symbol					models_strat/symbol_carthage_rebel.CAS
primary_colour					red 61, green 80, blue 204
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_carthage.tga
standard_index					7
logo_index					236
small_logo_index				250
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						parthia
culture						eastern
symbol						models_strat/symbol_parthia.CAS
rebel_symbol					models_strat/symbol_eastern_rebel.CAS
primary_colour					red 204, green 102, blue 61
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_parthia.tga
standard_index					8
logo_index					243
small_logo_index				255
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						pontus
culture						eastern
symbol						models_strat/symbol_pontus.CAS
rebel_symbol					models_strat/symbol_eastern_rebel.CAS
primary_colour					red 61, green 204, blue 202
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_pontus.tga
standard_index					9
logo_index					246
small_logo_index				260
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						gauls
culture						barbarian
symbol						models_strat/symbol_gaul.CAS
rebel_symbol					models_strat/symbol_barb_rebel.CAS
primary_colour					red 51, green 102, blue 31
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_gauls.tga
standard_index					10
logo_index					238
small_logo_index				252
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						no
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						germans
culture						barbarian
symbol						models_strat/symbol_german.CAS
rebel_symbol					models_strat/symbol_barb_rebel.CAS
primary_colour					red 204, green 61, blue 61
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_germans.tga
standard_index					11
logo_index					244
small_logo_index				256
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						no
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						britons
culture						barbarian
symbol						models_strat/symbol_briton.CAS
rebel_symbol					models_strat/symbol_barb_rebel.CAS
primary_colour					red 46, green 60, blue 153
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_britons.tga
standard_index					12
logo_index					234
small_logo_index				243
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						no
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						armenia
culture						eastern
symbol						models_strat/symbol_armenia.CAS
rebel_symbol					models_strat/symbol_eastern_rebel.CAS
primary_colour					red 153, green 46, blue 91
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_armenia.tga
standard_index					13
logo_index					235
small_logo_index				245
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						nabatea
culture						carthaginian
symbol						models_strat/symbol_numidia.CAS
rebel_symbol					models_strat/symbol_carthage_rebel.CAS
primary_colour					red 102, green 51, blue 31
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_nabatea.tga
standard_index					14
logo_index					250
small_logo_index				263
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						sparta
culture						greek
symbol						models_strat/symbol_greek_cities.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 153, green 46, blue 46
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_sparta.tga
standard_index					15
logo_index					233
small_logo_index				247
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						saba
culture						carthaginian
symbol						models_strat/symbol_dacia.CAS
rebel_symbol					models_strat/symbol_carthage_rebel.CAS
primary_colour					red 204, green 61, blue 121
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_saba.tga
standard_index					16
logo_index					241
small_logo_index				258
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						bosporan
culture						greek
symbol						models_strat/symbol_scythia.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 153, green 137, blue 46
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_scythia.tga
standard_index					17
logo_index					240
small_logo_index				254
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						spain
culture						barbarian
symbol						models_strat/symbol_spain.CAS
rebel_symbol					models_strat/symbol_barb_rebel.CAS
primary_colour					red 102, green 91, blue 31
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_spain.tga
standard_index					18
logo_index					248
small_logo_index				261
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						no
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						emporium
culture						greek
symbol						models_strat/symbol_thrace.CAS
rebel_symbol					models_strat/symbol_greek_rebel.CAS
primary_colour					red 102, green 204, blue 61
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_illyria.tga
standard_index					19
logo_index					249
small_logo_index				262
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						yes
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

faction						slave
culture						roman
symbol						models_strat/symbol_slaves.CAS
rebel_symbol					models_strat/symbol_slaves.CAS
primary_colour					red 179, green 179, blue 179
secondary_colour				red 0, green 0, blue 0
loading_logo					loading_screen/symbols/symbol128_slave.tga
standard_index					20
logo_index					237
small_logo_index				251
triumph_value					5
intro_movie					fmv/intros/julii_intro_final.wmv
victory_movie					fmv/victory/julii_outro_320x240.wmv
defeat_movie					fmv/lose/julii_eliminated.wmv
death_movie					fmv/death/death_julii_grass_320x240.wmv
custom_battle_availability			yes
can_sap						no
prefers_naval_invasions				yes

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;