;
; Ground Type Database
;

climate default
{
    types
    {
		grass		GRASS_SHORT	20.0	2000.0	0.0		70.0    2
		grass1	    	GRASS_LONG	50.0    900.0   0.0     	10.0	4	50.0
		forest		FOREST		50.0	800.0	0.0		10.0	3	150.0
		mud		MUD		20.0	2000.0  0.0		3.0	1
		rock		ROCK		MIN	MAX	0.0		90.0    0
		sand		SAND		MIN     20.0f	0.0		30.0    1
		snow		SNOW		1000.0	MAX	0.0		60.0    1
		shrub_base	SCRUB		20.0	2000.0	0.0		20.0	4	
    }

    base rock

    layer 0
    {
		sand
    }

    layer 1
    {
		grass
		forest
		shrub_base
    }
}

climate test_climate
{
	modifies default

	colour
	{
		220 220 150
	}
}

climate sandy_desert
{
	colour
	{
		220 200 180
	}

	types
	{
		caked_sand	SAND	MIN		MAX		0.0		2.0	4	
		sand		SAND	MIN		MAX		0.0		30.0    0
		sand_blown	SAND	MIN		MAX		0.0		10.0    4
		rock		ROCK	MIN		MAX		0.0		90.0	0
 	}

	prerequisites
	{
		caked_sand sand
		sand_blown sand
	}

	base rock

	layer 0
	{
		sand
	}

	layer 1
	{
		caked_sand
	}

	layer 2
	{
		sand_blown
	}
}

climate rocky_desert
{
	colour
	{
		180 150 50
	}

	types
	{
		pebble		ROCK	MIN		MAX		0.0		90.0	0
		rock		ROCK	MIN		MAX		0.0		90.0	0
		sand		SAND	MIN		MAX		0.0		90.0	0
	}

	base rock

	layer 0
	{
		pebble sand
	}
}

climate temperate_grassland_fertile
{
	modifies default

	colour
	{
		120 220 120
	}

	types
	{
		dirt		DIRT		20.0	1000.0	0.0		40.0	0
		grass		GRASS_SHORT	20.0	1000.0	0.0		40.0	2
		grass1		GRASS_LONG	100.0   900.0   0.0     	10.0	4	50.0
		forest		FOREST		50.0	900.0	0.0		10.0	4	150.0
		rock		ROCK		MIN	MAX	0.0		90.0	0
		sand		SAND		MIN     20.0f	0.0		30.0	1
		shrub_base	SCRUB		50.0	900.0	0.0		15.0	3	150.0
		snow		SNOW		1000.0	MAX	0.0		60.0	1
	}

	curve_dependent
	{
		forest
		sand
		snow
	}

	base rock

	layer 0
	{
		sand
	}

	layer 1
	{
		grass
		grass1
		forest
		shrub_base
	}
}

climate temperate_grassland_infertile
{
    modifies temperate_grassland_fertile

	colour
	{
		200 150 120
	}

}

climate temperate_forest_open
{
	modifies default

	colour
	{
		120 200 120
	}

}

climate temperate_forest_deep
{
	modifies default

	colour
	{
		100 150 100
	}
}

climate swamp
{
	colour
	{
		180 150 150
	}

	types
	{
		grass		GRASS_SHORT	20.0	2000.0	0.0		70.0	2
		grass1		GRASS_LONG	100.0   900.0   0.0     	10.0	4	50.0
		forest		FOREST		40.0	1000.0	0.0		10.0	4
		pebble		ROCK		MIN	MAX			0.0	90.0	0
		mud		MUD		20.0	2000.0  0.0		3.0	0
		sand		SAND		MIN     20.0f	0.0		30.0	1
		shrub_base	SCRUB		20.0	2000.0	0.0		20.0	4 	
	}

	prerequisites
	{
		grass1	grass
		mud     grass
	}

    	layer 0
    	{
		sand
		pebble
    	}

    	layer 1
	{
		grass
		forest
		shrub_base
	}

	layer 2
	{
		grass1
		mud
	}
}

climate highland
{
	modifies default

	colour
	{
		100 150 150
	}
}

climate alpine
{
	modifies default

	colour
	{
		50 200 200
	}

	types
	{
		snow		SNOW		900.0	MAX		0.0		40.0	1	100.0
	}

	curve_dependent
	{
		forest
		sand
		snow
	}
}

climate sub_arctic
{
	modifies default

	colour
	{
		200 200 120
	}

	types
	{
		pebble		ROCK		MIN		MAX		0.0		90.0	0
	}
}

climate semi_arid
{
	colour
	{
		200 180 120
	}

	types
	{
		grass		GRASS_SHORT	20.0	2000.0	0.0		70.0	2
		grass1		GRASS_LONG	100.0   900.0   0.0     	10.0	4	50.0
		forest		FOREST		40.0	1000.0	0.0		10.0	4
		pebble		ROCK		MIN	MAX			0.0	90.0	0
		mud		MUD		20.0	2000.0  0.0		3.0	0
		sand		SAND		MIN     20.0f	0.0		30.0	1
		shrub_base	SCRUB		20.0	2000.0	0.0		20.0	4 	
	}

	prerequisites
	{
		grass1	grass
		mud     grass
	}

	layer 0
	{
		sand
		pebble
	}

	layer 1
	{
		grass
		forest
		shrub_base
	}

	layer 2
	{
		grass1
		mud
	}
}
